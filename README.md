# hello

try to call the hello-module

# test

get the specific pseudo-version
```
go get -u gitlab.com/sgshopeefy/hello-module@ad3493859fef59b76f74
```

get a published version
```
go get -u gitlab.com/sgshopeefy/hello-module@v0.1.0
```