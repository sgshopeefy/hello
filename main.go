package main

import (
	"fmt"
	"log"

	"gitlab.com/sgshopeefy/hello-module/greetings"
	v2 "gitlab.com/sgshopeefy/hello-module/v2/greetings"
)

func main() {
	fmt.Println("V2:", v2.Hello(""))

	// Set properties of the predefined Logger, including
	// the log entry prefix and a flag to disable printing
	// the time, source file, and line number.
	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	msg, err := greetings.Hello("")
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(msg)
}
