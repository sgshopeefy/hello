module gitlab.com/sgshopeefy/hello

go 1.17

require gitlab.com/sgshopeefy/hello-module v0.1.0

require gitlab.com/sgshopeefy/hello-module/v2 v2.0.0
